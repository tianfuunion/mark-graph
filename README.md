# Open Knowledge Graph

#### 介绍

{ 开放知识图谱基于自由文本的开放域知识图谱构建。企业项目请看 [https://graph.open.tianfu.ink](https://graph.open.tianfu.ink)}

#### 软件架构

软件架构说明

## 安装方法

如果您通过composer管理您的项目依赖，可以在你的项目根目录运行：

        $ composer require tianfuunion/mark-graph

或者在你的`composer.json`中声明对 Mark Auth SDK For PHP 的依赖：

        "require": {
            "tianfuunion/mark-graph": "^2.0"
        }

然后通过`composer install`安装依赖。composer安装完成后，在您的PHP代码中引入依赖即可：

        require_once __DIR__ . '/vendor/autoload.php';

### License

- MulanPSL-2.0

### 联系我们

- [天府联盟官方网站：www.tianfuunion.cn](https://www.tianfuunion.cn)
- [天府授权中心官方网站：auth.tianfu.ink](https://auth.tianfu.ink)
- [天府联盟反馈邮箱：report@tianfuunion.cn](mailto:report@tianfuunion.cn)